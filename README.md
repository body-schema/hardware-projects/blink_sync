# blink_sync
In case of recording using multiple cameras connected to multiple computers, time synchronization is crutial.
One synchrnosization event in the beginning might not be enough to keep system in time sync for the duration of several minutes.
This device generates such events with fixed period. 

This device is but a simple blinker.
Device lights for ~1 second every ~30 seconds. 
The period is not precise nor is it stable.
The key here is that all the cameras see the blink at the same time.

<img src="photo_3.jpg" width="480">
<img src="photo_2.jpg" width="480">
<img src="photo_1.jpg" width="480">


## User manual
To turn the device on flip the switch on the side of the case.
The first light impulse is slightly longer than the others (due to electric ciruit used).

The device runs from standard 9V battery which is hidden inside the casing. 
To change the baterry first undo the screws holding the battery in place.

## Principle of function
Device uses 555 timer in form of astable flip-flop.
To achieve this short impulses of 1s over large period of 30s schottky diode is added to shortcut the resistor R2.
This design allows the capacitor C2 to be charged through resistor R1 and discharged through the resistor R2 unlike in classical 555 timer, where the capacitor C2 is charged through both resistors and discharged through one (R2).
Similar timing efect could be achieved also by simply switching the polarity of the output (light on when low on the output and swapping values of R1 and R2) however this would result in extremely long delay (~45 seconds) in the beginning.
Such behavior would lead to uncertainty whether the device is functional. 

To reduce the power consumtion, capacitor C2 is charged through R1 from output of the IC.
This has no effect on the function and when the capacitor C2 is being discharged, no current flows through R1.

